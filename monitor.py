# import webbrowser
# import pyglet
import PySimpleGUI as sg
import serial
import serial.tools.list_ports
# import time
# import ctypes
# import requests
# import matplotlib.pyplot as plt
# import threading
import sys

icon_def = b'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAD71JREFUeF7tm3dcE9kWx+/MBKUYQAL2AgRFVEQRRQQF+1N3fYpt9cmCbV0UBduuhY8F3dXVVcQCNkSsNAUERAU2CVUsqAg2DF0lkEaCgpCZeZ9hGT4hpEJ45bM7/+a2872/e+65595A4C/+QX9x+8HfAP5WgAICQ8wMRmMYHAtBeJxEQgkqFQpL/xdgmRsbG8Mw5glBuBsAYL7MmJg4DsVhGBxeKhQK1Rmv3CXQYjzDdeqst5WVJYPY7971BQBcRlFk/38TBJ1GJQwOM+5pLF7ssfqdnf140G/gIADDMKVWwOUyUx/MDQsO0kVRxELdcbYDQBq/8F/fv/rWfenEhoYG0FD/pSDmWjiUyUwdAQBgwjC2uajm83N1CGurDJ1GDQMAeP0aFPJiwqQpQwEAetJtp91LfPnLrm22AICVbJ74srr9tgNAp1EF3lt2fFy8wms40YiAzwW1AgFobGwETU2NnIjLlxqymClMNk/spW4nnS3XYvyCu9l5dbq6+v1l28tIe/Bq7/ZNxHg1Mp5oRx6AyyvWeNutWu87WrojAgKPWwOqq6qydvn+4IyiSE9111lnANBpVAJ0WALrcY4Bleok29bDDMbbXb7e1h0xXi4AK9MebjgOMVKfvBLAMNxTukNCDdxqTt2ezRtqOZwqf02k1lEIdBq1ZM/h42VuM+e4yraRyUh9s2erzzBVxhOOU9FkyXWCdBqV6fPTTgP37zwdpDuVNDWBspL3IJvFyL4UfEq/mC8a01HD1KlHzL6pWe+giGTGFxiG+0jXYT64Xxiww5fwSUplT/o0APBwNq/OT7ZfuQBIFfyR9wYFACDSlaqrPgFudXW+j9fSUao6V8dIZWXoNGrczoDDfWZ8M99RulzK3cT8Q/7bVPZPGr/GZzN08XSggM0TW6gFgChEp1FLfbbvxmfPX/RVT0+PWGPNX/2Xz6CirBTk5z3OCz522L4rIRDyPx8RW2E11GYS2T/zwV1WwI4txHJQa+b9duwpdpk2o2HRjEkT2TxxuwlXGAoTKgAYdAKHgJ3tmHHvfvTbWm49YtRIQoqEM+TVVIOnudmvLpw81iHvq4466DQqHpOS9cSERmtdigI+P2Xh9InjAQCxbJ54pbx2yJlfv21n0aLlnuNQFOXMGDeiNwThU95z65jSdVSeBcyNjc0RROIHAER4Y6OwW3cfDbawHP+fgEAAiExm/GHWu+/Uts5YMQTS+HW+294u9VzTvHQwDBdPd7ChdgiAdMeWJoZ+EIQHXopOyjWn0x27GgIB4MyVqNs2I0e5y860PCWQxnt5+77+fq1365ZZ/+VL8VwXe0uNloAiiZL78qXohIfm9CETugICsW0hCBqGUChT4hkPi/QNerTZjcixSUOAYSwIw2CGx2rvwpUbfJ2lx/+mIP/h+u+X9GXzxOZqO0EV3rk5OAmNupNjYTXUSZsQWg47DJqZqenlmKTKHoaGE5SNRQqC0VLPNRnrfLe1Okyy3k8bVlc9ycmMVHsbVNNBNUO4GBWfZWll7awNCKTxQ6yH6oZcv9WAIJQ20aiicREQGPeTdNyXeRAnxDbfx8ry7BXzZk5UdEBS6QTVUcKFiNhM+lAbl85AINev/Tgn7pGQUB0YhgerMxHKykiamt7NdLTtpSgIIup2CkBLvNCshPM3Y9OtrG0mdwQCafzcBUuebvHf7wBBkFFnjccw7NO0sSM4MMAhCYa4aRQKa9o56RjP3rjNGjpsuKsmEMgz/vJVa1+u8dnabv1qOhai/Ify8qce82fSIRyUKTNeKwogB0hCCLl2i2k9fISbOhDIOlv8A5K/cV8yWxNjUYkks77+S09dPX0ahULpI+TzcgtfvhDcCD1n8brghTUEQfvfc0X7VLXZ6SUg3QFp0Jkr0QybkbZTlEGwNDH0hSD8xJHg0DiHCc6yqS1V4wbHDu4tS7od2cZPQDh4gUM4E0UpJzqcEVLZs4oCJITT4VFpw21HTZMHAQBAxPJep8JupoywGzND0z7PBh6tiL4SyifkDQAwBgAIO5qb0KoCZJfDmfCIVBvb0dPlQKg9fjb8Kd162FSqkWb+7vbN8Menjx4aiqKIeUeNlgbeJQCkdwdylmUh/HLyXD6NZjqqd59+wKhnm7yLQkHcjYt59nuAvyUMY27aykl2GQBpCCdDrz8YOWbsTEUQevXpC4x7mrQznCivr28A9PT1QW4WK2fnxnXDtWm8VncBRdNmZWq4D8fxvUGh1+7ZjnH4hyIIZr17g54mps3NSCQSkJwQyxg82EJHV0/PpU4syt7248qJAIAFbJ44TlOfoax8lyqADHBmfjNf/CAxbmDghWvJdmMdZiuCYNqrFzChmYGQ44eYMdfD3XAcCPf/frJm77ZNQ1QlQDoKpcsAkMavWO393mXqdAduVXWe/1Zv+8DzV+7aOYyfowjCnZgb2Q8Sbo8I3W9RcDdDACLvCZzV3dM7AqFLAJBbod+ufc/mLfpujEgoBFWfPgDOp4+P9m7bOP7Y2ctJY8ZPmCsLYfqceW+z0hL7Xthr/rq7DtyczLjN4GffvMsn1v4UbTm+Lt0FSOPJNU92VisUEADAx4ryrIAdm51/DwlLsHd0+paEkJ56T5QQFQruBw97aUyFnSs5jaC2DmuufofJz7maxLfpCghaVYCVaY9AHIf8yIORrCSFAh5xsQI+lJcyD+zc6nYkOPSOwwTneRHhFwtjwgIH3j1t/dqwx58zT3zSEBJYgodXEnnDtA1BawCI66seVKr7+Zu3X/XpN1BhEoO4XKnhcEAZuyjt0J4d03y27y6NvPibSeKpIUVUfWSsLDRpCEnpwtzLCVxrbULoNICW9FVg3/4DFp67EfumB5U6TpUz4vNqiLsF8DCDWXEv5qxxQpBVsYEeYiddr5tT8nO0/FI9+iHaSRpCYobwcfgd7hBtQegUADKDY2Nr2/vYuTCRnp5B6/2BKgiE7JMjgwbdPm5VbqCLEDc8bT7E5iALMZvqirIDc2Qh3MusfRwaX6MVCB0GQBo/cbIb9UBgsA4MQ4NUGU3+nhwX8zz6YgD91nH6R71uiEJoyiA8yK59ciG2xqqzSugQAHKPn/XNfOFP+w+bQBAwVNf42KjrOfeuHxkZeZTO0e0GW8nKXlIeVo99iGpNaSuF8FD09MKtanpnIGgMoDX3/uOmV9//sJ4IT9X+iC2vuOhN3s8b1tqf8x8UP8ne8J/SleH+S3IodD8nSfGpbKzyZmvbyiCkPRLlnY2uJg5IHYoTNAJApq82bt/NXrDMo53HVkVCLBKBTx8qAI/Hzd+9ad0ouRAGLMumWG6ciJaczUIrrrTm95VBYDwWPQuOqrboCAS1AZABzoHjpzOd3aa7qDJW0e8EhPISdu42by/rrw1fjeVD8MimWHpPlJScy8IqwtWC8McT4fOQSK65phDUAkCmr4KvRTOGDbed0lHjiXqf60R5ngvm0Pk8LnGqIy4qw+RCGOiZRbFY5ywpC83EykJbgStTAjOv9sWZmzWDNYGgEgAR4CAI4nUx8k7uYEt6m3t6TUFIJGjBD8vm9Sxls1PJN0aksuRCGLw6kzJ4tQtafjkDLT3fmjFWBiH9aW3+qYiaQepCUAqAMN7YxMT9auz9AgMqVSOHJwsHw7DyzWs9GwryHtfLpqqVQjBfm0kZtNIFrbiagZaEqAUh85koP+hGtVoQFAIgjB9obr7w7PXbZXp6+iMJgzAM4zU2NlTr6urbaDL7OI4L92zx4WUz0+oU5emVQUAsvDOQgR6T0Irr6WjJmclk3zrDf2FCplPc5AVLWc9FL09crx6oSgkKnsg0Z3H8EjOeNN/MVnM+5R3Zt6tbXm5OMwiaWa+qFWt+fP/PxcuJsLe7MhgQBGGnjx5k3bpx1RJFkdHKEplKIVj6pCMDlk+WVEamY8VBrRCULYesF+KCE9c4A5RBaAegJbYvORIcyiTy9QXPn6VvWrVsMg6BaBjgwYACCkETWIwB6ICtnX31ybAbxLsbhRDCz57OCj9/eqS6uTylEKx805F+Syfjn2JYTUXHW1+NKVNCzktx4fErnP6KIMh7J+hlQjMLiknJMCQkv2rR3M9lJcVhxfy6drcsdBq14FDQuRrHSa7tbmUJVcRF3Ug/eTjATl3jSSWREC74D4p3lgmWEKvN6Ui/xZPRT7EstOhoKwRlSohO4b+IvC/A5b1qk/tQcta38/v9vP/wjNcv83N8PBfXvefVzZQnc0szwzlWVtYhFyLi2p0DHmVnxO/wWUtEeh1KZCpVwpDtLKTvAlf00x0WWnRYIYQKTiMQEUkVHCtZe7CMJhShvrJvG+UBYP6871DjrHkLZpz87cDj+KhrWe+5dZvlAWhZLoLknOfs7t116WSZwhd5DzauXE5A0/jpqnQ/yiHsYCF957miVYks9N2vciHUl0U6lX1sBI1NACQwBblXknhVbJ64zTWcXABrfDZzl69atzA24mrKqaO/lLG54rXyAFj1MqLjKPY+9ckrHgzDNKIMn1sdt2jmZKKTThkvuxzkxQnIUH8W0meOK8q5x0TfBrQuw+blYDzWqDFn9mjRZxRUVDUBDv9rms+hirFsnrjNLYwcAD1OjB43we74uXC3D+UlWR7zZxujKOIiz3vTadSf7B2dPH4PCWveHYQCQZr7NCfijBCvzcfUypSgY72HCfX+hxtek8Jser23nS9CURy8Kf0KGr6irz38S2xkH0rJU8B8XV3d2HjWk486OpR+xw7uyU28Fd1d1oH8+XwOLbmRlJrbp+8AxzqxKHfxrElEfK9V49VRgo7NfiZkNsMN5zKYTa92t4NQVN4AKjjNCnBg88TEZWrrJzcOIF6J/mv1uorVGza7NH5tLN7pu67u2aPsJgCgQoCDGhwCQyEAvv016BxjwiTXKRiG5i2bM7VbTTWHx+aJ5e4ImgROisoqVQIRFBnZGxOyl61fXPkVRN3nq+cDiMotZ/5n5N09juN1KUnxT4uLinR5XA5liPXwBkcXVzDYku5MPFT4Ybk7rbToXaOq1xhdDUFR+8/ffi5ZE1Bm8qUe81O5C8hKboyD45XfzlzsT9HRmSbbwZvC/PD1HktcIRzU/ieMV2c5yI5RKEbBxt/KX+UVfm56zxe3U4fSw1DLq3Hi7ydGdmPH5Y13nlzb04Rm8O5NISfjj7QxvOqqkQDgQShK2aeNu3pNFEIuhw1Le93b8F2vgQAH7RKrjRL8nf/JSlFiZu2QljC83R+/VB6HiUG1ZIKIra31pSUEQUyJBL6s7lMUTYxTt2zLBMWZGCG1axf0YjmO0kf60LoNr+R8fZ6cJTINi+e6EA+lIATzUnStphYAdQf03yjXkp32ggHuRbxslxoDi/inm6p/tfzfA+gs9L8BdJbg/3v9v7wC/g0ReZXIcGeExAAAAABJRU5ErkJggg=='
sg.theme('SystemDefaultForReal')

reflash_icon = b'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAhxJREFUOE91UztoVEEUPfftEmKxAYvY2iwaefetLIIabAwRxWAsxA+yFiKoIBLBTkTUQrALCoI/EEyjxRb+0YimEBRi4+YOGtjCQixsJCbEyIN3ZMLs8ngkU90595z7mzuCwqlWq329vb17AVQBJME9A6C9tLT0qt1u/8lLJH+J43iPiNwAsLEYONxnSZ5zzr3u+LsBVPUKgMurCP8BmAOwLvivmpnnYzmAqtYAfFlB/JjkWxG5R/ISgAURGQ+8zWbWksHBwTXz8/MvAezsliXisiw7LCKHAPhMU2Y25P1JktwledJjlUplRJIkOUXyTlGcZdnvcrn8meRaAFvM7GuHo6pPAYyKSENUdQLAsVz5o2b2XFUPAGj6bM65+/n2kiQZIPkQwBsf4DuA9YHwzcw2hVJPk7wd8Ekz2+3tOI4fiMjxgDd9gF8A+gPQ7VVVLwLY5XGSP51zjTBw38pAPsB7P0CSZ51zt/KlquoRAI9IXnfOXajVatuzLPuY41zzFXjRmZBp2Dn3ztv1er0/TdMZEflbKpWGFhcXF3p6el4A2JobeEPiON4hIh8COBdFkbZarR+dSZM8KiLTAG4CGMlln07TdLizSGMA/Ar784nkhIj4ysZIDkdRtI9kqbBoQ2Y2lV9lFgj+0/StsJ0eaprZQW8UP9MTEdm/imgZJjnunDvfnUORrKonSG4TkQ3+2QFk4Z/MAnhmZpN5zX92cOR5v9jyvgAAAABJRU5ErkJggg=='
toggle_btn_off = b'iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABmJLR0QA/wD/AP+gvaeTAAAED0lEQVRYCe1WTWwbRRR+M/vnv9hO7BjHpElMKSlpqBp6gRNHxAFVcKM3qgohQSqoqhQ45YAILUUVDRxAor2VAweohMSBG5ciodJUSVqa/iikaePEP4nj2Ovdnd1l3qqJksZGXscVPaylt7Oe/d6bb9/svO8BeD8vA14GvAx4GXiiM0DqsXv3xBcJU5IO+RXpLQvs5yzTijBmhurh3cyLorBGBVokQG9qVe0HgwiXLowdy9aKsY3g8PA5xYiQEUrsk93JTtjd1x3siIZBkSWQudUK4nZO1w3QuOWXV+HuP/fL85klAJuMCUX7zPj4MW1zvC0Ej4yMp/w++K2rM9b70sHBYCjo34x9bPelsgp/XJksZ7KFuwZjr3732YcL64ttEDw6cq5bVuCvgy/sje7rT0sI8PtkSHSEIRIKgCQKOAUGM6G4VoGlwiqoVd2Za9Vl8u87bGJqpqBqZOj86eEHGNch+M7otwHJNq4NDexJD+59RiCEQG8qzslFgN8ibpvZNsBifgXmFvJg459tiOYmOElzYvr2bbmkD509e1ylGEZk1Y+Ssfan18n1p7vgqVh9cuiDxJPxKPT3dfGXcN4Tp3dsg/27hUQs0qMGpRMYjLz38dcxS7Dm3nztlUAb38p0d4JnLozPGrbFfBFm79c8hA3H2AxcXSvDz7/+XtZE1kMN23hjV7LTRnKBh9/cZnAj94mOCOD32gi2EUw4FIRUMm6LGhyiik86nO5NBdGRpxYH14bbjYfJteN/OKR7UiFZVg5T27QHYu0RBxoONV9W8KQ7QVp0iXdE8fANUGZa0QAvfhhXlkQcmjJZbt631oIBnwKmacYoEJvwiuFgWncWnXAtuVBBEAoVVXWCaQZzxmYuut68b631KmoVBEHMUUrJjQLXRAQVSxUcmrKVHfjWWjC3XOT1FW5QrWpc5IJdQhDKVzOigEqS5dKHMVplnNOqrmsXqUSkn+YzWaHE9RW1FeXL7SKZXBFUrXW6jIV6YTEvMAUu0W/G3kcxPXP5ylQZs4fa6marcWvvZfJu36kuHjlc/nMSuXz+/ejxgqPFpuQ/xVude9eu39Jxu27OLvBGoMjrUN04zrNMbgVmOBZ96iPdPZmYntH5Ls76KuxL9NyoLA/brav7n382emDfHqeooXyhQmARVhSnAwNNMx5bu3V1+habun5nWdXhwJZ2C5mirTesyUR738sv7g88UQ0rEkTDlp+1wwe8Pf0klegUenYlgyg7bby75jUTITs2rhCAXXQ2vwxz84vlB0tZ0wL4NEcLX/04OrrltG1s8aOrHhk51SaK0us+n/K2xexBxljcsm1n6x/Fuv1PCWGiKOaoQCY1Vb9gWPov50+fdEqd21ge3suAlwEvA14G/ucM/AuppqNllLGPKwAAAABJRU5ErkJggg=='
toggle_btn_on = b'iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABmJLR0QA/wD/AP+gvaeTAAAD+UlEQVRYCe1XzW8bVRCffbvrtbP+2NhOD7GzLm1VoZaPhvwDnKBUKlVyqAQ3/gAkDlWgPeVQEUCtEOIP4AaHSI0CqBWCQyXOdQuRaEFOk3g3IMWO46+tvZ+PeZs6apq4ipON1MNafrvreTPzfvub92bGAOEnZCBkIGQgZOClZoDrh25y5pdjruleEiX+A+rCaQo05bpuvJ/+IHJCSJtwpAHA/e269g8W5RbuzF6o7OVjF8D3Pr4tSSkyjcqfptPDMDKSleW4DKIggIAD5Yf+Oo4DNg6jbUBlvWLUNutAwZu1GnDjzrcXzGcX2AHw/emFUV6Sfk0pqcKpEydkKSo9q3tkz91uF5aWlo1Gs/mYc+i7tz4//19vsW2AU9O381TiioVCQcnlRsWeQhD3bJyH1/MiFLICyBHiuzQsD1arDvypW7DR9nzZmq47q2W95prm+I9fXfqXCX2AF2d+GhI98Y8xVX0lnxvl2UQQg0csb78ag3NjEeD8lXZ7pRTgftmCu4864OGzrq+5ZU0rCa3m+NzXlzvoAoB3+M+SyWQuaHBTEzKMq/3BMbgM+FuFCDBd9kK5XI5PJBKqLSev+POTV29lKB8rT0yMD0WjUSYLZLxzNgZvIHODOHuATP72Vwc6nQ4Uiw8MUeBU4nHS5HA6TYMEl02wPRcZBJuv+ya+UCZOIBaLwfCwQi1Mc4QXhA+PjWRkXyOgC1uIhW5Qd8yG2TK7kSweLcRGKKVnMNExWWBDTQsH9qVmtmzjiThQDs4Qz/OUSGTwcLwIQTLW58i+yOjpXDLqn1tgmDzXzRCk9eDenjo9yhvBmlizrB3V5dDrNTuY0A7opdndStqmaQLPC1WCGfShYRgHdLe32UrV3ntiH9LliuNrsToNlD4kruN8v75eafnSgC6Luo2+B3fGKskilj5muV6pNhk2Qqg5v7lZ51nBZhNBjGrbxfI1+La5t2JCzfD8RF1HTBGJXyDzs1MblONulEqPDVYXgwDIfNx91IUVbAbY837GMur+/k/XZ75UWmJ77ou5mfM1/0x7vP1ls9XQdF2z9uNsPzosXPNFA5m0/EX72TBSiqsWzN8z/GZB08pWq9VeEZ+0bjKb7RTD2i1P4u6r+bwypo5tZUumEcDAmuC3W8ezIqSGfE6g/sTd1W5p5bKjaWubrmWd29Fu9TD0GlYlmTx+8tTJoZeqYe2BZC1/JEU+wQR5TVEUPptJy3Fs+Vkzgf8lemqHumP1AnYoMZSwsVEz6o26i/G9Lgitb+ZmLu/YZtshfn5FZDPBCcJFQRQ+8ih9DctOFvdLIKHH6uUQnq9yhFu0bec7znZ+xpAGmuqef5/wd8hAyEDIQMjAETHwP7nQl2WnYk4yAAAAAElFTkSuQmCC'

port_list_init = list(serial.tools.list_ports.comports())

# print(port_list_name)
# if len(port_list) <= 0:
#     print("无串口设备。")
# else:
#     print("可用的串口设备如下：")
#     for comport in port_list:
#         print(list(comport))

def make_mainwindow():
    layout = [
             [
                sg.Text("端口号1",font='微软雅黑'),sg.Combo(port_list_init,size = 20,bind_return_key=True,font='微软雅黑',key='_port1',	enable_events=True,),
                sg.Button(image_data=reflash_icon, key='reflashport1', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0, metadata=False),
                sg.Text('波特率1',font='微软雅黑',),sg.Combo(['115200', '9600','38400','460800','921600'],default_value=460800,size = 8, key= '_baudrate1',bind_return_key=True,font='微软雅黑',),
                sg.Text('打开/关闭串口1',font='微软雅黑'),
                sg.Button(image_data=toggle_btn_off, key='-serial1-open-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0, metadata=False),
                sg.VSep(),                
                sg.Text('激光器参数串口',font='微软雅黑'),
             ],
             [sg.HSep()],
             [
                sg.Text("端口号2",font='微软雅黑'),sg.Combo(port_list_init,size = 20,bind_return_key=True,font='微软雅黑',key='_port2',	enable_events=True,),
                sg.Button(image_data=reflash_icon, key='reflashport2', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0, metadata=False),
                sg.Text('波特率2',font='微软雅黑',),sg.Combo(['115200', '9600','38400','460800','921600'],default_value=115200,size = 8, key= '_baudrate2',bind_return_key=True,font='微软雅黑',),
                sg.Text('打开/关闭串口2',font='微软雅黑'),
                sg.Button(image_data=toggle_btn_off, key='-serial2-open-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0, metadata=False),
                sg.VSep(),                
                sg.Text('波形参数串口',font='微软雅黑'),
             ],
             [sg.HSep()],
             [
                 sg.Text('一级',font='微软雅黑'),
                sg.Button(image_data=toggle_btn_off, key='-TOGGLE1-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0, auto_size_button = True,),
                sg.Text('二级',font='微软雅黑'),
                sg.Button(image_data=toggle_btn_off, key='-TOGGLE2-', button_color=(sg.theme_background_color(), sg.theme_background_color()),border_width=0, ),
                sg.Text('三级',font='微软雅黑'),
                sg.Button(image_data=toggle_btn_off, key='-TOGGLE3-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0,),
                sg.Text('四级',font='微软雅黑'),
                sg.Button(image_data=toggle_btn_off, key='-TOGGLE4-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0,),
                sg.Text('一键开启/关闭',font='微软雅黑'),
                sg.Button(image_data=toggle_btn_off, key='-TOGGLEALL-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0,),
                sg.VSep(),
                sg.Text('调试模式',font='微软雅黑'),
                sg.Button(image_data=toggle_btn_off, key='-DEBUG-MODE-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0,),
             ],
             [sg.HSep()],
             [
                sg.Text('一级电流',font='微软雅黑'),
                sg.Text('000.00',font='微软雅黑',p=0),
                sg.Text('mA',font='微软雅黑',p=0),
                sg.Input('',s=6,font='微软雅黑'),
                sg.Text('mA',font='微软雅黑',p=0),
                sg.VSep(),
                sg.Text('一级温度',font='微软雅黑'),
                sg.Text('-273.15',font='微软雅黑',p=0),
                sg.Text('℃',font='微软雅黑',p=0),
                sg.VSep(),
                sg.Text('一级光功率',font='微软雅黑'),
                sg.Text('00000',font='微软雅黑',p=0),
                sg.Text('mW',font='微软雅黑',p=0),
                sg.Text('LD1_info'),
             ],
             [
                sg.Text('二级电流',font='微软雅黑'),
                sg.Text('000.00',font='微软雅黑',p=0),
                sg.Text('mA',font='微软雅黑',p=0),
                sg.Input('',s=6,font='微软雅黑'),
                sg.Text('mA',font='微软雅黑',p=0),
                sg.VSep(),
                sg.Text('二级温度',font='微软雅黑'),
                sg.Text('-273.15',font='微软雅黑',p=0),
                sg.Text('℃',font='微软雅黑',p=0),

                sg.VSep(),
                sg.Text('二级光功率',font='微软雅黑'),
                sg.Text('00000',font='微软雅黑',p=0),
                sg.Text('mW',font='微软雅黑',p=0),
                sg.Text('LD2_info'),
             ],
             [
                sg.Text('三级电流',font='微软雅黑'), 
                sg.Text('000.00',font='微软雅黑',p=0),
                sg.Text('A',font='微软雅黑',p=0),
                sg.Input('',s=6,font='微软雅黑'),
                sg.Text('A',font='微软雅黑',p=0),
                sg.VSep(),
                sg.Text('三四级温度',font='微软雅黑'),
                sg.Text('-273.15',font='微软雅黑',p=0),
                sg.Text('℃',font='微软雅黑',p=0),
                sg.Input('',s=6,font='微软雅黑'),
                sg.Text('℃ ~',font='微软雅黑',p=0),
                sg.Input('',s=6,font='微软雅黑'),
                sg.Text('℃',font='微软雅黑',p=0),
                sg.Text('LD3_info'),
             ],
             [
                sg.Text('四级电流',font='微软雅黑'),
                sg.Text('000.00',font='微软雅黑',p=0),
                sg.Text('A',font='微软雅黑',p=0),
                sg.Input('',s=6,font='微软雅黑'),
                sg.Text('A',font='微软雅黑',p=0),
                sg.VSep(),
                sg.Text('热沉温度',font='微软雅黑'),
                sg.Text('-273.15',font='微软雅黑',p=0),
                sg.Text('℃',font='微软雅黑',p=0),
                sg.Input('',s=6,font='微软雅黑'),
                sg.Text('℃',font='微软雅黑',p=0),
                sg.Text('LD4_info'),
             ], 
             [
                sg.Button('确定',font='微软雅黑',s=800,button_color='#8BB7F0',mouseover_colors=('#8BB7F0','#C8D1DB'),),
              ],            
             [sg.HSep()],
             [sg.StatusBar('无异常',font='微软雅黑')],
             [sg.HSep()],
             [
               sg.Text('开启/关闭驱动器',font='微软雅黑'),
                sg.Button(image_data=toggle_btn_off, key='-TOGGLE-drive-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0, metadata=False),
             ],
             [sg.HSep()],
             [
                sg.Text('EOM宽度',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Text('ns',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('ns',font='微软雅黑',p=0),
                sg.Text('EOM与AOM延迟',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Text('ns',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('ns',font='微软雅黑',p=0),
                sg.VSep(),
                sg.Text('AOM幅值',font='微软雅黑'), 
                sg.Text('0.00',font='微软雅黑',p=0),
                sg.Text('V',font='微软雅黑',p=0),
                sg.Input('',s=5,font='微软雅黑'),
                sg.Text('V',font='微软雅黑',p=0),
             ],
             [
                sg.Text('EOM_START_DELAY',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('EOM_END_DELAY',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),

                sg.Text('AOM_START_DELAY',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('AOM_END_DELAY',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),     
             ],
             [
                sg.Text('D1',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D2',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D3',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D4',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D5',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D6',font='微软雅黑'),
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),  
             ],
             [
                sg.Text('D7',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D8',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D9',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D10',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D11',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D12',font='微软雅黑'),
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),  
             ],
             [
                sg.Text('D13',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D14',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D15',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D16',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D17',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D18',font='微软雅黑'),
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),  
             ],
             [
                sg.Text('D19',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D20',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D21',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D22',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D23',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D24',font='微软雅黑'),
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),  
             ],
             [
                sg.Text('D25',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D26',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D27',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D28',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D29',font='微软雅黑'), 
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),
                sg.Text('D30',font='微软雅黑'),
                sg.Text('000',font='微软雅黑',p=0),
                sg.Input('',s=4,font='微软雅黑'),  
             ],
             [
                sg.Button('确定',font='微软雅黑',s=800,button_color='#8BB7F0',mouseover_colors=('#8BB7F0','#C8D1DB'),),
             ],            
             [sg.HSep()],
             [sg.StatusBar('无异常',font='微软雅黑')],
    ]
    window = sg.Window('预研测风雷达调试上位机',layout,size=(1200,830),finalize=True,icon=icon_def,auto_size_text=True,auto_size_buttons= True,)
    return window
mainwindow = make_mainwindow()

while True:
    event, values = mainwindow.read(timeout=100)
    port_list = list(serial.tools.list_ports.comports())
    
   #  print (event, values)
    if event == sg.WIN_CLOSED or event == '-exit-':
       sys.exit()
    elif event == '-TOGGLE1-': 
        mainwindow['-TOGGLE1-'].metadata = not mainwindow['-TOGGLE1-'].metadata
        mainwindow['-TOGGLE1-'].update(image_data=toggle_btn_on if mainwindow['-TOGGLE1-'].metadata else toggle_btn_off)
    elif event == '-TOGGLE2-': 
        mainwindow['-TOGGLE2-'].metadata = not mainwindow['-TOGGLE2-'].metadata
        mainwindow['-TOGGLE2-'].update(image_data=toggle_btn_on if mainwindow['-TOGGLE2-'].metadata else toggle_btn_off)
    elif event == '-TOGGLE3-': 
        mainwindow['-TOGGLE3-'].metadata = not mainwindow['-TOGGLE3-'].metadata
        mainwindow['-TOGGLE3-'].update(image_data=toggle_btn_on if mainwindow['-TOGGLE3-'].metadata else toggle_btn_off)
    elif event == '-TOGGLE4-': 
        mainwindow['-TOGGLE4-'].metadata = not mainwindow['-TOGGLE4-'].metadata
        mainwindow['-TOGGLE4-'].update(image_data=toggle_btn_on if mainwindow['-TOGGLE4-'].metadata else toggle_btn_off)
    elif event == '-TOGGLEALL-': 
        mainwindow['-TOGGLEALL-'].metadata = not mainwindow['-TOGGLEALL-'].metadata
        mainwindow['-TOGGLEALL-'].update(image_data=toggle_btn_on if mainwindow['-TOGGLEALL-'].metadata else toggle_btn_off)
    elif event == '-TOGGLE-drive-': 
        mainwindow['-TOGGLE-drive-'].metadata = not mainwindow['-TOGGLE-drive-'].metadata
        mainwindow['-TOGGLE-drive-'].update(image_data=toggle_btn_on if mainwindow['-TOGGLE-drive-'].metadata else toggle_btn_off)
    elif event == '-DEBUG-MODE-': 
        mainwindow['-DEBUG-MODE-'].metadata = not mainwindow['-DEBUG-MODE-'].metadata
        mainwindow['-DEBUG-MODE-'].update(image_data=toggle_btn_on if mainwindow['-DEBUG-MODE-'].metadata else toggle_btn_off)
    elif event == '-serial1-open-': 
        mainwindow['-serial1-open-'].metadata = not mainwindow['-serial1-open-'].metadata
        mainwindow['-serial1-open-'].update(image_data=toggle_btn_on if mainwindow['-serial1-open-'].metadata else toggle_btn_off)
    elif event == '-serial2-open-': 
        mainwindow['-serial2-open-'].metadata = not mainwindow['-serial2-open-'].metadata
        mainwindow['-serial2-open-'].update(image_data=toggle_btn_on if mainwindow['-serial2-open-'].metadata else toggle_btn_off)
        print(mainwindow['-serial2-open-'].metadata)








